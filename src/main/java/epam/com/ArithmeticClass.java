package epam.com;

public class ArithmeticClass {

  private double result;

  public double divideTwoNumber(double firstNumber, double secondNumber)
      throws MyException {
    if (secondNumber == 0) {
      throw new MyException("Divide by zero");
    } else {
      result = firstNumber / secondNumber;
    }
    return result;
  }

}
