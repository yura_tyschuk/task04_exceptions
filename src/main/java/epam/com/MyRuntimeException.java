package epam.com;

public class MyRuntimeException extends RuntimeException {
    MyRuntimeException() { }
    MyRuntimeException(String message) {
      super(message);
    }

    MyRuntimeException(String message, Throwable cause) {
      super(message, cause);
    }

    MyRuntimeException(Throwable cause){
      super(cause);
    }
}
