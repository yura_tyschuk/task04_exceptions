package epam.com;

public class Student extends Man {
  private final int course;

  public Student() {
    super();
    course = 1;
  }

  public Student(int course, int age, int weight, String male, String  name) {
    super(age, weight, male, name);
    this.course = course;
  }

  public Student (Student obj) {
    super(obj);

    this.course = obj.course;
  }

  public final int getCourse() {
    return course;
  }

  public final void Display() {
    super.Display();

    System.out.println("Course: " + course);
  }
}
