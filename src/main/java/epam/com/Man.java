package epam.com;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

public class Man implements AutoCloseable {
  private int age;
  private int weight;
  private String male;
  private String name;
  private PrintWriter file;

  public Man() {
    age = 0;
    weight = 0;
    male = "None";
    name = "None";
  }

  public Man(int age, int weight, String male, String  name) {
    this.age = age;
    this.weight = weight;
    this.male = male;
    this.name = name;
    try {
     file = new PrintWriter("students.txt");
     file.println("Age: " + age);
     file.println("Weight : " + weight);
     file.println("Male: " + male);
     file.println("Name:  " + name);
    } catch (FileNotFoundException e) {
      System.out.println("File not found");
    }
  }

  public Man(Man obj) {
    this.age = obj.age;
    this.weight = obj.weight;
    this.male = obj.male;
    this.name = obj.name;
  }

  public void SetAge(int age) {
    this.age = age;
  }

  public final int getAge() {
    return age;
  }

  public void setName(String name) {
    this.name = name;
  }

  public final String getName() {
    return name;
  }

  public void setMale(String male) {
    this.male = male;
  }

  public final String getMale() {
    return male;
  }

  public void setWeight(int weight) {
    this.weight = weight;
  }

  public final int getWeight() {
    return weight;
  }


  public void Display() {
    System.out.println("Name: " + name);
    System.out.println("Age:  " + age);
    System.out.println("Male: " + male);
    System.out.println("Weight: " + weight);
  }

  public void close() throws IOException {
    if(file != null) {
      file.println();
      System.out.println("Class closed");
      file.println("Closed");
      file.close();
    }
  }
}

