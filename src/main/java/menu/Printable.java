package menu;

import epam.com.MyException;

public interface Printable {
    void print() throws MyException;
}
