package menu;

import controller.Controller;
import epam.com.ArithmeticClass;
import epam.com.Man;
import epam.com.MyException;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
  private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    ArithmeticClass arithmeticClass;

  public MyView() {
      controller = new Controller();
      arithmeticClass = new ArithmeticClass();
      menu = new LinkedHashMap<>();
      methodsMenu = new LinkedHashMap<>();
      menu.put("1" , "1 -Divide two numbers");
      menu.put("2", "2 - Closeable class");
      menu.put("Q", "Quit");

      methodsMenu.put("1", this::divideTwoNumbers);
      methodsMenu.put("2", this::closeableClass);
    }

    private void outputMenu() {
      System.out.println("\n Menu: ");
      for(String str : menu.values()) {
        System.out.println(str);
      }
    }

    private void divideTwoNumbers() throws MyException {
      System.out.println("Print first number: ");
      double firstNumber = input.nextDouble();
      System.out.println("Print second number: ");
      double secondNumber = input.nextDouble();
      try {
        double result = controller.divideTwoNumber(10, 0);
        System.out.println(result);
      } catch(MyException m) {
        System.out.println(m.getMessage());
      }
    }

    private void closeableClass() {
      try(Man man = new Man(19, 56, "male", "yura")) {
        controller.closeableClass(man);
      } catch(IOException e) {
        e.printStackTrace();
      }
    }

    public void show() {
      String keyMenu;
      do {
        outputMenu();
        System.out.println("Please select menu point: ");
        keyMenu = input.nextLine().toUpperCase();
        try {
          methodsMenu.get(keyMenu).print();

        } catch (Exception e) {

        }
      } while(!keyMenu.equals("Q"));
    }


}
