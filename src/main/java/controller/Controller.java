package controller;

import epam.com.ArithmeticClass;
import epam.com.Man;
import epam.com.MyException;
import java.io.IOException;

public class Controller {

  ArithmeticClass arithmeticClass;
  Man man;
  public Controller() {
    arithmeticClass = new ArithmeticClass();
    man = new Man(19, 56, "male", "Yura");
  }

  public double divideTwoNumber(double firstNumber, double secondNumber)
      throws MyException {
    return arithmeticClass.divideTwoNumber(firstNumber, secondNumber);
  }

  public void closeableClass(Man man) throws IOException {
    man.Display();
    man.close();
  }


}
